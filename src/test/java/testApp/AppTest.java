package testApp;

import org.junit.Assert;
import org.junit.Test;

import java.util.logging.Logger;


/**
 * Unit test for simple App.
 */
public class AppTest {

    private static final Logger log = Logger.getLogger(AppTest.class.getName());

    @Test
    public void testAdd() {
        App app = new App();
        log.info("Zacatek testu scitani");
        Assert.assertTrue(8 == app.add(4, 4));
        log.info("Konec testu scitani");

        log.info("Zacatek testu odcitnani");
        Assert.assertTrue(2 == app.minus(4, 2));
        log.info("Konec testu odcitani");

        log.info("Zacatek testu nasobeni");
        Assert.assertTrue(4 == app.multiply(4, 2));
        log.info("Konec testu nasobeni");
    }
}

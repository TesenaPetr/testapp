package testApp;

/**
 * Hello world!
 */
public class App {

    public double add(double a, double b) {
        return a + b;
    }

    public double minus(double a, double b) {
        return a - b;
    }

    public double multiply(double a, double b) {
        return a * b;
    }

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
